<?php

session_start();
header ("content-type : application/json");

if(!is_array($_SESSION["todos"]))
{
    $_SESSION["todos"] =array();
}

if(isset ($_POST["texte"]) && $_POST ["texte"]!= "")
{
    $todo = array("id" => uniqid(), "texte" => $_POST["texte"], "date" => time(), "termine" => false);
    $_SESSION["todos"][$todo["id"]]=$todo;

}

// Sauvegarder dans la Session

// Affichage un JSON
echo json_encode(array("success" => true));

?>