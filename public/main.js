var app =new Vue ({
    el: '.container',

    created: function (){
        console.log("Démarrage TODO-APP");
    },

    data:{
        taches:[]
    },

    beforeMount: function (){
        this.recupererListe();
    },

    methods: {
        ajout: function () {
            var contenu = document.getElementById("texte").value;
            if(contenu == ""){
                swal("Oops","Vous devez spécifier du texte…" , "error" );
            }else{
                var form = new FormData();
                form.append('texte', contenu);
                fetch("API/creation.php", {
                    method: "POST",
                    body: form,
                    credentials: 'same-origin'
                })
                    .then(function(response){
                        return response.json();
                    })
                    .then(function(response) {
                        if(response.success){
                            app.recupererListe();
                        }else{
                        }
                    })
                    .catch(function(error) {
                        console.log('Récupération impossible: ' + error.message);
                    });
                swal("Ajout effectué avec succès", "Cliquer sur OK pour quitter !", "success");

            }
        },

        recupererListe : function (){
            fetch('API/liste.php', {method: "GET", credentials: 'same-origin'})
                .then(function(response){
                    // On décode le JSON, et on continue
                    //json_decode($_SESSION["todos"]);
                    return response.json();
                })

                .then(function(response) {
                    // Votre retour est ICI

                    app.taches = response;
                    console.log(response);
                })
                .catch(function(error) {
                    console.log('Récupération impossible: ' + error.message);
                });
        },

        terminer:function (id) {
            var finish = document.getElementById("termine").value;
            if(finish == id){
                //swal("Oops","Vous devez spécifier du texte…" , "error" );
            }else{
                form.append('termine', finish);
                fetch("API/terminer.php", {
                    method: "POST",
                    body: form,
                    credentials: 'same-origin'
                })
                    .then(function(response){
                        return response.json();
                    })
                    .catch(function(error) {
                        console.log('Récupération impossible: ' + error.message);
                    });
            }
        },

        supprimer:function (id) {
            fetch('API/suppression.php', {method: "GET", credentials: 'same-origin'})
                .then(function(response){
                    // On décode le JSON, et on continue
                    //json_decode($_SESSION["todos"]);
                    return response.json();
                })

                .then(function(response) {
                    // Votre retour est ICI

                    app.taches = response;
                    console.log(response);
                })
                .catch(function(error) {
                    console.log('Récupération impossible: ' + error.message);
                });

        }

    }
})

