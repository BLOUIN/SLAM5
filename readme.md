Cette application a été conçue à l’aide du Framework JavaScript VueJS. 

Cette application a pour but de mémoriser les tâches que nous avons à faire. 
Une fois ces tâches mémorisées, il sera possible de les mettre à l’état terminé. 
Après les avoir misent en « mode » terminé, il sera possible de les supprimés. 

Ces opérations peuvent se répéter autant de fois que l’on le désirera. 
